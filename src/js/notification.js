new Vue({
    el: '#notifications',
    data: {
        notifications: {
 			0: {
                title: 'ТОО "Бревно" проводит акцию',
                text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem.  Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum',
                background: {
                	background: 'url(https://uc.uxpin.com/files/868058/860444/1337636_1-976f1c.jpg)'
                },
                visible: false
            },
            1: {
                title: 'ТОО "Бревно" проводит акцию',
                text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem.  Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum',
                background: {
                	background: 'url(https://uc.uxpin.com/files/868058/860444/1337636_1-976f1c.jpg)'
                },
                visible: true
            }
        }
    },
    methods: {
    },
    computed: {
    }
});