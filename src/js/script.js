window._ = require('lodash');
var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
var labelIndex = 0;
function initMap() {
    MAP =  google.maps;
    map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 43.2441631, lng:76.919435},
        zoom: 13,
        panControl: false,
        zoomControl: true,
  		fullscreenControl: false,
        zoomControlOptions: {
            position: google.maps.ControlPosition.RIGHT_CENTER,
            style: google.maps.ZoomControlStyle.LARGE
        },
        mapTypeControl: false,
        scaleControl: false,
        // streetViewControl: false,
        // scrollwheel: false,
        overviewMapControl: false
    });
    if ((localStorage.user_lat!=null)&&(localStorage.user_lng!=null)) {
        var pos = {
                    lat: parseFloat(localStorage.user_lat),
                    lng: parseFloat(localStorage.user_lng)
                };
        map.setCenter(pos);
        map.zoom = 18;
        addMarker(pos, map);
    }else{
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                localStorage.setItem('user_lat', pos.lat)
                localStorage.setItem('user_lng', pos.lng)
                map.setCenter(pos);
                map.zoom = 20;
                addMarker(pos, map);
            }, function() {
                handleLocationError(true, infoWindow, map.getCenter());
            });
        } else {
            // Browser doesn't support Geolocation
            handleLocationError(false, infoWindow, map.getCenter());
        }
    }
}
function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
        'Error: The Geolocation service failed.' :
        'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map);
}
// Adds a marker to the map.
function addMarker(location, map, icon) {
    var marker = new google.maps.Marker({
        position: location,
        icon: {
            url: 'images/resizeApi.png',
            scaledSize: { width: 32, height: 32},
            anchor: { x: 16, y: 16 },
          },
        map: map
    });
}
