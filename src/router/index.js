/* eslint-disable */
import Vue from 'vue'
import Router from 'vue-router'

import Login from '@/components/Login'
import Index from '@/components/Index'
import Order from '@/components/Order'
import List from '@/components/List'
import Footer from '@/components/include/Footer'
import Header from '@/components/include/Header'
import Map from '@/components/include/Map'
import * as VueGoogleMaps from 'vue2-google-maps'
import VueSocketio from 'vue-socket.io'
import vueResource from 'vue-resource'

Vue.use(VueSocketio, 'http://46.36.218.84:3000')
Vue.use(vueResource)
Vue.use(Router)
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyCCezqXG8vCpEP2KlLfAGZLz_LFPURk_RQ',
    libraries: 'places', // This is required if you use the Autocomplete plugin
  },
})
Vue.component('my-footer', Footer)
Vue.component('my-map', Map)
Vue.component('my-header', Header)
export default new Router({
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/index',
      name: 'Index',
      component: Index
    },
    {
      path: '/order',
      name: 'order',
      component: Order
    },
    {
      path: '/list',
      name: 'list',
      component: List
    }
  ],
  mode: 'history'
})
